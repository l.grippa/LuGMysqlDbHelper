package eu.grippaweb.providers.mappers;

import eu.grippaweb.ILuGRowMapper;
import eu.grippaweb.providers.data.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Map object with table fields for each row
 */
public class EmployeeMapper implements ILuGRowMapper<Employee> {
    @Override
    public Employee map(ResultSet rs, int index) throws SQLException {
        Employee emp = new Employee();
        emp.setId(rs.getInt("id"));
        emp.setFirstName(rs.getString("first_name"));
        emp.setLastName(rs.getString("last_name"));
        emp.setSalary(rs.getInt("salary"));
        emp.setBirthDate(rs.getDate("birth_date"));
        return emp;
    }
}
