package eu.grippaweb.providers;

import com.mysql.cj.jdbc.MysqlDataSource;
import eu.grippaweb.LuGMysqlDbHelper;
import eu.grippaweb.exceptions.LuGNoConfigurationException;
import eu.grippaweb.mappers.DateTimeMapper;
import eu.grippaweb.mappers.FloatMapper;
import eu.grippaweb.mappers.HashMapMapper;
import eu.grippaweb.mappers.LongMapper;
import eu.grippaweb.mappers.StringMapper;
import eu.grippaweb.providers.data.Employee;
import eu.grippaweb.providers.mappers.EmployeeMapper;
import eu.grippaweb.supports.LuGMysqlConfiguration;
import eu.grippaweb.supports.LuGMysqlParameter;
import org.junit.*;

import java.sql.SQLException;
import java.util.*;

/// Connection string template jdbc:mysql://[host1][:port1][,[host2][:port2]]...[/[database]] »
// [?propertyName1=propertyValue1[&propertyName2=propertyValue2]...]
// see https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-reference-configuration-properties.html
// the db structure for test
// schema name:  mysqltest
// table:

// CREATE TABLE mysqltest.employee(
//  id INT(11) NOT NULL AUTO_INCREMENT,
//  first_name VARCHAR(20) DEFAULT NULL,
//  last_name VARCHAR(20) DEFAULT NULL,
//  salary INT(11) DEFAULT NULL,
//  birth_date DATE DEFAULT NULL,
//  PRIMARY KEY (id)
//)
//ENGINE = INNODB
//ROW_FORMAT = DYNAMIC;

public class LuGMysqlTest {
    /**
     * jdbc url with username and password
     */
    private String MYSQL_URL_COMPLETE="jdbc:mysql://localhost:3306/mysqltest?user=user&password=user&useSSL=true&serverTimezone=Europe/Rome";
    /**
     * Mysql Data source
     */
    private MysqlDataSource dataSource;

    ////////////////// INIT TEST SECTION ///////////////////////

    /**
     * Example for connect with datasource as singleton
     */
    @Before
    public void createDatasource(){
        //////////////////// CREATE DATASOURCE ///////////////////
        dataSource = new MysqlDataSource();
        dataSource.setDatabaseName("mysqltest");
        dataSource.setUser("user");
        dataSource.setPassword("user");
        dataSource.setPort(3306);
        dataSource.setServerName("localhost");

        ///////////////////////////////////////////////////////////<
        //// connect as singleton
        LuGMysqlConfiguration config = new LuGMysqlConfiguration();
        // set datasource to configuration
        config.setDataSource(dataSource);
        LuGMysqlProvider.initMyHelperAsSingleton(config);
        try {
            LuGMysqlProvider.getMyHelperAsSinglton().connect();
            Assert.assertTrue(LuGMysqlProvider.getMyHelperAsSinglton().isConnected());
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        //////////////////////////////////////////////////////////
    }

    /**
     * Disconnect singleton
     */
    @After
    public void disconnectSingleton(){
        if(LuGMysqlProvider.getMyHelperAsSinglton() != null){
            try {
                LuGMysqlProvider.getMyHelperAsSinglton().close();
            } catch (SQLException e) {
                e.printStackTrace();
                Assert.fail();

            }
        }
    }
    //////////////////////////// TEST SECTION //////////////////////////
    /**
     * Connect with datasource and no singleto instance
     */
    @Test
    public void connectTestDatasource() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(dataSource);
        try {
            dbHelper.connect();
            // connect database
            Assert.assertTrue("credential test fail",dbHelper.isConnected());
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test connection
     */
    @Test
    public void connectTestNoCredentials() {
        // build helper
        String MYSQL_URL_NO_CREDENTIALS = "jdbc:mysql://localhost:3306/mysqltest?useSSL=true&serverTimezone=Europe/Rome";
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_NO_CREDENTIALS,"user","user");
        // init employ entity
        Employee emp = null;
        try {
            // connect database
            Assert.assertTrue("credentila test fail",dbHelper.connect());
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    @Test
    public void testFloatMapper() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                Float floatData = dbHelper.queryAndMap("SELECT 1.988",new Object[]{},new FloatMapper());
                Assert.assertNotNull("Float Data", floatData);
               // System.out.println("Long Data", String.valueOf(longData));
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }
    
    @Test
    public void testLongMapper() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                Long longData = dbHelper.queryAndMap("SELECT "+Long.MAX_VALUE,new Object[]{},new LongMapper());
                Assert.assertNotNull("Long Data", longData);
               // System.out.println("Long Data", String.valueOf(longData));
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }
    /**
     * Test for fetch data and map into simple object.
     * This ezample query whit no named paramiter.
     *
     */
    @Test
    public void testStringMapper() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                String firsthName = dbHelper.queryAndMap("SELECT first_name FROM employee WHERE first_name=? and last_name=? LIMIT 1",new Object[]{"Luciano","Grippa"},new StringMapper());
                Assert.assertNotNull("The Employee Luciano Grippa not found", firsthName);
                System.out.println("|  Firsth: "+firsthName);
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test hash map
     */
    @Test
    public void hashmapRecordTest() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        try {
            // connect database
            if (dbHelper.connect()) {
                // successfully connect
                // query single employee
                List<HashMap<String,Object>> resultlist = dbHelper.queryAndMapList("SELECT * FROM employee;", new Object[]{}, new HashMapMapper());
                Assert.assertNotNull("No records founfef", resultlist != null && resultlist.size()>0);
                if(resultlist != null && resultlist.size()>0){
                    Iterator rs=  resultlist.iterator();
                    /*
                      the result list of hashmap contains key value pairs,
                      where keys are field's name and values are values of field in specific row index
                    */
                    while (rs.hasNext()){
                        HashMap<String,Object> mapEntry = (HashMap<String, Object>) rs.next();
                        System.out.println("ID: "+mapEntry.get("id")+" ");
                        System.out.print("First name: "+mapEntry.get("first_name")+" ");
                        System.out.print("Last name: "+mapEntry.get("last_name")+" ");
                        System.out.print("Salary: "+mapEntry.get("salary")+" ");
                        System.out.print("Birth date: "+mapEntry.get("birth_date")+" | ");
                    }
                }

            } else {
                Assert.assertTrue("No database connected", false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        } finally {
            if (dbHelper != null) {
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }
    /**
     * Test for fetch data and map into simple object.
     * This ezample query whit no named paramiter.
     *
     */
    @Test
    public void testDateTimeMapper() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        Employee emp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                Date birthDate = dbHelper.queryAndMap("SELECT birth_date FROM employee WHERE first_name=? and last_name=? LIMIT 1",new Object[]{"Luciano","Grippa"},new DateTimeMapper());
                Assert.assertNotNull("The Employee Luciano Grippa not found", birthDate);
                System.out.println("|  BIRTH DATE: "+birthDate);

            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test for fetch data and return json results
     * This example query with no named paramiter.
     *
     */
    @Test
    @Ignore
    public void queryJsonWithParams() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        String jemp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                List<LuGMysqlParameter> params = new ArrayList<>();
                params.add(new LuGMysqlParameter("@firstName","Lu"));
                jemp = dbHelper.queryJson("SELECT * FROM employee WHERE first_name LIKE concat('%',@firstName,'%')",params,new EmployeeMapper());
                Assert.assertTrue(jemp != null && !jemp.isEmpty());
                System.out.println(jemp);
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test for fetch data and return json results
     * This example query with no named paramiter.
     *
     */
    @Test
    @Ignore
    public void queryJson() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        String jemp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                jemp = dbHelper.queryJson("SELECT * FROM employee",new Object[]{},new EmployeeMapper());
                Assert.assertTrue(jemp != null && !jemp.isEmpty());
                System.out.println(jemp);
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test for fetch data and map into simple object.
     * This ezample query whit no named paramiter.
     *
     */
    @Test
    public void queryAndMap() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        Employee emp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                emp = dbHelper.queryAndMap("SELECT * FROM employee WHERE first_name=? and last_name=? LIMIT 1",new Object[]{"Luciano","Grippa"},new EmployeeMapper());
                Assert.assertNotNull("The Employee Luciano Grippa not found", emp);
                Employee e = emp;
                System.out.println("ID: "+e.getId()+"|  BIRTH DATE: "+emp.getBirthDate()+ "|   FIRST NAME: "+e.getFirstName()+
                        "|    LAST NAME: "+e.getLastName()+"|      SALARY: "+e.getSalary());

            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Test for fetch data and map into simple object.
     * This ezample query whit  named paramiter.
     *
     */
    @Test
    public void queryAndMapNamedParamiters() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        Employee emp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // build paramiters
                List<LuGMysqlParameter> params = new ArrayList<LuGMysqlParameter>();
                params.add(new LuGMysqlParameter("@firstname","Luciano"));
                params.add(new LuGMysqlParameter("@lastname","Grippa"));
                // query single employee
                emp = dbHelper.queryAndMap("SELECT * FROM employee WHERE first_name=@firstname and last_name=@lastname LIMIT 1",params,new EmployeeMapper());
                Assert.assertNotNull("The Employee Luciano Grippa not found", emp);
                Employee e = emp;
                System.out.println("ID: "+e.getId()+"|   FIRST NAME: "+e.getFirstName()+
                        "|    LAST NAME: "+e.getLastName()+"|      SALARY: "+e.getSalary());

            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    @Test
    public void queryAndMapList() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        List<Employee> emp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // successfully connect
                // query single employee
                emp = dbHelper.queryAndMapList("SELECT * FROM employee WHERE first_name LIKE concat('%',?,'%')",new Object[]{"a"},new EmployeeMapper());
                Assert.assertNotNull("The Employees not found", emp);
                if(emp != null && emp.size()>0) {
                    Assert.assertTrue("The Employees not found",emp.size()>0);
                    System.out.println("The Employees are: ");
                    for(int j =0;j<emp.size();j++){
                        Employee e = emp.get(j);
                                               System.out.println("ID: "+e.getId()+"|   FIRST NAME: "+e.getFirstName()+
                                "|    LAST NAME: "+e.getLastName()+"|      SALARY: "+e.getSalary());

                    }
                }
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    @Test
    public void queryAndMapListWithNamedParamiters() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init employ entity
        List<Employee> emp = null;
        try {
            // connect database
            if (dbHelper.connect()) {
                // successfully connect
                List<LuGMysqlParameter> params = new ArrayList<LuGMysqlParameter>();
                params.add(new LuGMysqlParameter("@firstNameStart", "l"));
                emp = dbHelper.queryAndMapList("SELECT * FROM employee WHERE first_name LIKE concat('%',@firstNameStart,'%') OR last_name LIKE concat('%',@firstNameStart,'%')", params, new EmployeeMapper());
                // emp = dbHelper.queryAndMapList("SELECT * FROM employee;",params,new EmployeeMapper());
                Assert.assertNotNull("The Employees not found", emp);
                if (emp != null && emp.size() > 0) {
                    Assert.assertTrue("The Employees not found", emp.size() > 0);
                    System.out.println("The Employees are: ");
                    for (int j = 0; j < emp.size(); j++) {
                        Employee e = emp.get(j);
                        System.out.println("ID: " + e.getId() + "|   FIRST NAME: " + e.getFirstName() +
                                "|    LAST NAME: " + e.getLastName() + "|      SALARY: " + e.getSalary());

                    }
                } else {
                    Assert.fail("Empty data");
                }
            } else {
                Assert.assertTrue("No database connected", false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    /**
     * Query with singleton connection
     */
    @Test
    public void queryAsSingleton(){
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.getMyHelperAsSinglton();
        // init employ entity
        Employee emp = null;
        try {
            // connect database
            if(dbHelper.isConnected()){
                // successfully connect
                // query single employee
                emp = dbHelper.queryAndMap("SELECT * FROM employee WHERE first_name=? and last_name=? LIMIT 1",new Object[]{"Luciano","Grippa"},new EmployeeMapper());
                Assert.assertNotNull("The Employee Luciano Grippa not found", emp);
                Employee e = emp;
                System.out.println("ID: "+e.getId()+"|   FIRST NAME: "+e.getFirstName()+
                        "|    LAST NAME: "+e.getLastName()+"|      SALARY: "+e.getSalary());
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void update() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        boolean isUpdate = false;
        try {
            if(dbHelper.connect()){
                String sqlUpdate ="UPDATE employee SET first_name=? WHERE id=?";
                int numRows = dbHelper.update(sqlUpdate,new Object[]{"Derek",4});
                isUpdate = numRows>0;
                Assert.assertTrue("Update non eseguito",isUpdate);
            }
            else
            {
                Assert.fail("Db not connected");
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        finally {
            if(dbHelper != null) {
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    @Test
    public void updateWithAutocommitFalse() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        boolean isUpdate = false;
        try {
            if(dbHelper.connect()){
                dbHelper.setAutocommit(false);
                String sqlUpdate ="UPDATE employee SET first_name=? WHERE id=?";
                int numRows = dbHelper.update(sqlUpdate,new Object[]{"Jorge",2});
                isUpdate = numRows>0;
                Assert.assertTrue("Update non eseguito",isUpdate);
                if(isUpdate)
                    dbHelper.commit();
                else
                    dbHelper.rollback();
            }
            else
            {
                Assert.fail("DB not connected");
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        finally {
            if(dbHelper != null) {
                try {
                    dbHelper.setAutocommit(true);
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }

    @Test
    public void updatWithNamedValueParamitersWithAutocommitFalse() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        boolean isUpdate = false;
        try {
            if(dbHelper.connect()){
                dbHelper.setAutocommit(false);
                List<LuGMysqlParameter> params = new ArrayList<LuGMysqlParameter>();
                params.add(new LuGMysqlParameter("@firstName","Jorge"));
                params.add(new LuGMysqlParameter("@id",2));
                String sqlUpdate ="UPDATE employee SET first_name=@firstName WHERE id=@id";
                int numRows = dbHelper.update(sqlUpdate,params);
                isUpdate = numRows>0;
                Assert.assertTrue("Update non eseguito",isUpdate);
                if(isUpdate)
                    dbHelper.commit();
                else
                    dbHelper.rollback();
            }
            else
            {
                Assert.fail("DB nont connected");
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        finally {
            if(dbHelper != null) {
                try {
                    dbHelper.setAutocommit(true);
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }
}