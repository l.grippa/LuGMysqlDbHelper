/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Luciano Grippa
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFT
 */
package eu.grippaweb;

import eu.grippaweb.exceptions.LuGNoConfigurationException;
import eu.grippaweb.supports.LuGMysqlConfiguration;
import eu.grippaweb.supports.LuGMysqlParameter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Helps to perfom db operation.
 -- ex: connect to db and list all employee --
 * <code>
 *       // jdbc url
 *       String MYSQL_URL_COMPLETE="jdbc:mysql://localhost:3306/mysqltest?user=user&password=user&useSSL=true&serverTimezone=Europe/Rome";
 *       // build helper
 *       LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
 *        // init employ entity
 *        List<Employee> emp = null;
 *        try {
 *              // connect database
 *              if(dbHelper.connect()){
 *                  // successfully connect
 *                  // query single employee
 *                  emp = dbHelper.queryAndMapList("SELECT * FROM employee WHERE first_name LIKE concat('%',?,'%')",new Object[]{"a"},
 *                                                  new EmployeeMapper());
 *
 *                  if(emp != null && emp.size()>0) {
 *                      System.out.println("The Employees are: ");
 *                      for(int j =0;j<emp.size();j++){
 *                          Employee e = emp.get(j);
 *                          System.out.println("ID: "+e.getId()+"|   FIRST NAME: "+e.getFirstName()+
 *                                             "|    LAST NAME: "+e.getLastName()+"|      SALARY: "+e.getSalary());
 *
 *                      }
 *                  }
 *              }
 *              else
 *              {
 *                   System.out.println("No database connected",false);
 *              }
 *            } catch (LuGNoConfigurationException e) {
 *                  e.printStackTrace();
 *            } catch (SQLException e) {
 *                  e.printStackTrace();
 *              }finally {
 *                  if(dbHelper != null){
 *                      try {
 *                          dbHelper.close();
 *                       } catch (SQLException e) {
 *                              e.printStackTrace();
 *                       }
 *                  }
 *             }
 * </code>
 * @author Luciano Grippa
 * @since 1.0.0
 */
public interface LuGMysqlDbHelper {
    /**
     * Setup configuration
     * @param configuration
     */
    void setConfiguration(LuGMysqlConfiguration configuration);

    /**
     * Connect to database
     * @return
     * @throws LuGNoConfigurationException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    boolean connect() throws LuGNoConfigurationException, SQLException;

    /**
     * Close connection
     *
     * @throws SQLException
     */
    void close() throws SQLException;

    /**
     * Set autocommit for transactional query
     * @param isAutocommit
     * @throws SQLException
     */
    void setAutocommit(boolean isAutocommit) throws SQLException;

    /**
     * Commit operation
     * @throws SQLException
     */
    void commit() throws SQLException;

    /**
     * Rollback operation
     * @throws SQLException
     */
    void rollback() throws SQLException;

    /**
     * Get if connection started
     * @return
     */
    boolean isConnected();

    /**
     * Prepare sql statment
     *
     * @param sqlQuery
     * @return
     */
    PreparedStatement prepare(String sqlQuery) throws SQLException;

    /**
     * Execute query on alive connection and map single result in object
     *
     * @param sqlQuery  query to execute
     * @param params    array of object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    <T> T queryAndMap(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Execute query on alive connection and map single result in object
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  query to execute
     * @param params    list of LuGMysqlParameter object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    <T> T queryAndMap(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Execute query on alive connection and map results in List of object
     *
     * @param sqlQuery  query to execute
     * @param params    array of object that contains parameters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    <T> List<T> queryAndMapList(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Execute query on alive connection and map results in List of object
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  query to execute
     * @param params    list of LuGMysqlParameter object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    <T> List<T> queryAndMapList(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Execute query and put results in json formatted string
     * The result json representing an array of objects
     *
     * @param sqlQuery      Query to execute
     * @param params        list of LuGMysqlParameter object that contains paramiters
     * @param mapper        mapper object
     * @return json string representing the result of the query
     * @since 1.0.1
     * @throws SQLException
     */
    String queryJson(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Execute query and put results in json formatted string
     * The result json representing an array of objects
     *
     * @param sqlQuery      Query to execute
     * @param params        array of object that contains paramiters
     * @param mapper        mapper object
     * @return json string representing the result of the query
     * @since 1.0.1
     * @throws SQLException
     */
    String queryJson(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException;

    /**
     * Performs insert update or delete operations
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  update, insert, delete query
     * @param params    list of LuGMysqlParameter objects
     * @return numbers of affected rows
     *
     * @throws SQLException
     */
    int update(String sqlQuery, List<LuGMysqlParameter> params) throws SQLException;

    /**
     * Performs insert update or delete operations
     *
     * @param sqlQuery  update, insert, delete query
     * @param params   array of objects contains parameters values
     *
     * @return numbers of affected rows
     * @throws SQLException
     */
    int update(String sqlQuery, Object[] params) throws SQLException;

    /**
     * Call a stored procedure
     *
     * @param procedureName stored procedure name
     * @param params    paramiters
     * @return resultser
     *
     * @throws SQLException
     */
    ResultSet callStoredProcedure(String procedureName, Object[] params) throws SQLException;
}
