/**
 *   MIT License
 *
 *   Copyright (c) 2018 Luciano Grippa
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFT
 */
package eu.grippaweb.supports;

import javax.sql.DataSource;

/**
 * Model data for connection
 *
 * @author Luciano Grippa
 * @since 1.0.0
 */
public class LuGMysqlConfiguration {
    /**
     * Connection string
     * the format is :
     * Connection string template jdbc:mysql://[host1][:port1][,[host2][:port2]]...[/[database]] »
     * [?propertyName1=propertyValue1[&propertyName2=propertyValue2]...]
     */
    private String connectionString;
    /**
     * Datasource for connection
     */
    private DataSource dataSource;
    /**
     * Username
     */
    private  String username;
    /**
     * Password
     */
    private String password;
    /**
     * Driver for mysql connector
     */
    private String driver = "com.mysql.cj.jdbc.Driver";
    /**
     * Get data source
     * @return DataSource
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Set Data source
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Get connection string like
     * jdbc:mysql://127.0.0.1:3306/yourdatabase
     *
     * @return
     */
    public String getConnectionString() {
        return connectionString;
    }

    /**
     * Set connection string like
     *jdbc:mysql://127.0.0.1:3306/yourdatabase
     *
     * @return
     */
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    /**
     * Get username
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get password
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set dn password
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get db driver the default value is "com.mysql.cj.jdbc.Driver"
     * @return
     */
    public String getDriver() {
        return driver;
    }

    /**
     * Set db driver
     * @param driver
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }
}
