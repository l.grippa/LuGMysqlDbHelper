package eu.grippaweb.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import eu.grippaweb.ILuGRowMapper;

public class LongMapper implements ILuGRowMapper<Long> {

	@Override
	public Long map(ResultSet rs, int index) throws SQLException {
		return rs != null ? rs.getLong(1):null;
	}

}
