/**
 * Copyright (c) 2017 Luciano Grippa
 * <p>
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * <p>
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
package eu.grippaweb.mappers;

import eu.grippaweb.ILuGRowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Map result record in HashMap object
 * The hashmap object is structurated :
 * HashMap<String,Object> record
 *
 * @author Luciano Grippa
 */
public class HashMapMapper implements ILuGRowMapper<HashMap<String,Object>> {
    /**
     * Method call from interface that map resulset object
     *
     * @param rs    result set object
     * @param index position inside result set
     * @return
     * @throws SQLException
     */
    @Override
    public HashMap<String, Object> map(ResultSet rs, int index) throws SQLException {
        HashMap<String, Object> record = new HashMap<String, Object>();
        try{
            ResultSetMetaData metadata = rs.getMetaData();
            int totalCols = metadata.getColumnCount();
            for (int i=0;i<totalCols;i++){
                record.put(metadata.getColumnName(i+1),rs.getObject(i+1));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return record;
    }
}
