/**
 *   MIT License
 *
 *   Copyright (c) 2018 Luciano Grippa
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFT
 */
package eu.grippaweb.mappers;

import eu.grippaweb.ILuGRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Model Mapper for Obect data type.
 * Assume that the blob field is the first column of select
 *
 * Object d = new db.queryAndMap("SELECT any_object_field FROM table LIMIT 1",new Object[]{} , new ObjectMapper());
 *
 * @author Luciano Grippa
 * @since 1.0.0
 */
public class ObjectMapper implements ILuGRowMapper<Object> {
    /**
     * Function called for mapping Object object
     *
     * @param rs        Resulset data
     * @param index     index of row
     * @return Object object
     * @throws SQLException
     */
    public Object map(ResultSet rs, int index) throws SQLException {
        return rs != null?rs.getObject(1):null;
    }
}
