package eu.grippaweb.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import eu.grippaweb.ILuGRowMapper;

public class FloatMapper implements ILuGRowMapper<Float> {
	@Override
	public Float map(ResultSet rs, int index) throws SQLException {
		return rs != null ? rs.getFloat(1) : null ;
	}
}
