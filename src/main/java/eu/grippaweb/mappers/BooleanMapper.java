package eu.grippaweb.mappers;

import eu.grippaweb.ILuGRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanMapper implements ILuGRowMapper<Boolean> {

    @Override
    public Boolean map(ResultSet rs, int index) throws SQLException {
        return rs != null?rs.getBoolean(1):false ;
    }
}