/**
 *   MIT License
 *
 *   Copyright (c) 2018 Luciano Grippa
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFT
 */
package eu.grippaweb.providers;

import com.google.gson.Gson;
import eu.grippaweb.ILuGRowMapper;
import eu.grippaweb.LuGMysqlDbHelper;
import eu.grippaweb.exceptions.LuGNoConfigurationException;
import eu.grippaweb.supports.LuGMysqlConfiguration;
import eu.grippaweb.supports.LuGMysqlParameter;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * LuGMysqlDbHelper interface implementation
 *
 * @author Luciano grippa
 * @since  1.0.0
 */
@SuppressWarnings("unchecked")
class LuGMysql implements LuGMysqlDbHelper {
    /**
     * Configuration object
     */
    private LuGMysqlConfiguration configuration;

    /**
     * Connection object
     */
    private Connection connection;

    /**
     * Current statment object
     */
    private PreparedStatement preparedStmt;

    /**
     * True if connected
     */
    private boolean connected;

    /**
     * Set configuration object
     *
     * @param configuration
     */
    public void setConfiguration(LuGMysqlConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Get configuration object
     *
     * @return
     */
    public LuGMysqlConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * Execute connectio to server
     *
     * @return true if connected successfully
     * @throws LuGNoConfigurationException
     * @throws SQLException
     */
    public boolean connect() throws LuGNoConfigurationException, SQLException {
        connected = false;
        // check if configuration is initialized
        if(getConfiguration() != null){

            if(getConfiguration().getDataSource() == null &&
                     getConfiguration().getConnectionString() != null &&
                    !getConfiguration().getConnectionString().isEmpty()) {

                try {
                    Class.forName(getConfiguration().getDriver());

                    if (!getConfiguration().getConnectionString().contains("password")) {
                        connection = DriverManager.
                                getConnection(getConfiguration().
                                                getConnectionString(),
                                        getConfiguration().getUsername(),
                                        getConfiguration().getPassword());
                    } else {
                        connection = DriverManager.getConnection(getConfiguration().getConnectionString());
                    }
                    connected = true;
                } catch (ClassNotFoundException e) {
                    throw new LuGNoConfigurationException("No com.mysql.jdbc.Driver found");
                }

            }
            else if(getConfiguration().getDataSource() != null) {
                // use datasource
                connection = getConfiguration().getDataSource().getConnection();
                connected = true;
            }
            else
            {
                throw new LuGNoConfigurationException("Connection is not configured properly");
            }
        }
        else
        {
            throw new LuGNoConfigurationException("The configuration is required");
        }
        return connected;
    }

    /**
     * Close connection
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        if(preparedStmt != null &&!preparedStmt.isClosed())
            preparedStmt.close();

        if(connection != null && isConnected())
            connection.close();

    }

    /**
     * set the autocommit to the value passed as a parameter
     *
     * @param isAutocommit
     * @throws SQLException
     */
    public void setAutocommit(boolean isAutocommit) throws SQLException {
       if (connection != null && isConnected()){
           connection.setAutoCommit(isAutocommit);
       }
    }

    /**
     * Perform commit operation
     *
     * @throws SQLException
     */
    public void commit() throws SQLException {
        if (connection != null && isConnected()){
            connection.commit();
        }
    }

    /**
     * Perform rollback operation
     *
     * @throws SQLException
     */
    public void rollback() throws SQLException {
        if (connection != null && isConnected()){
            connection.rollback();
        }
    }

    /**
     * True if connection still alive
     *
     * @return
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * Prepare statement for sql command
     *
     * @param sqlQuery
     * @return PreparedStatement object or null
     * @throws SQLException
     */
    @Override
    public PreparedStatement  prepare(String sqlQuery) throws SQLException {
        CallableStatement stmt = null;
        if(isConnected()){
            stmt = connection.prepareCall(sqlQuery);
            setPreparedStmt(stmt);
        }
        else
        {
            new SQLException("No connection started");
        }
        return stmt;
    }

    /**
     * Execute query on alive connection and map single result in object
     *
     * @param sqlQuery  query to execute
     * @param params    array of object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    public <T> T queryAndMap(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException {
        T row = null;
        if(mapper != null) {
            // check if connected
            if (isConnected()) {
                PreparedStatement stmt = prepare(sqlQuery);
                if (params != null && params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        stmt.setObject(i + 1, params[i]);
                    }
                }
                ResultSet results = stmt.executeQuery();

                if (results != null) {
                    if (results.next()) {
                        row = (T) mapper.map(results, 0);
                    }
                    if(!results.isClosed())
                    	results.close();
                }

            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }
        return row;
    }

    /**
     * Execute query on alive connection and map single result in object
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  query to execute
     * @param params    list of LuGMysqlParameter object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    public <T> T queryAndMap(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException {
        T row = null;
        if(mapper != null) {
            HashMap<String,int[]> paramsCollection = new HashMap<String,int[]>();
            // normalize query paramiters an collect index
            sqlQuery = normalizeSql(sqlQuery,paramsCollection);
            // check if connected
            if (isConnected()) {
                PreparedStatement  stmt = prepare(sqlQuery);
                buildParamitersIndex(params, paramsCollection, stmt);
                ResultSet results = stmt.executeQuery();
                if (results != null) {
                    if (results.next()) {
                        row = (T) mapper.map(results, 0);
                    }
                }
                if(!results.isClosed())
                	results.close();
            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }
        return row;
    }

    /**
     * Execute query on alive connection and map results in List of object
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  query to execute
     * @param params    list of LuGMysqlParameter object that contains paramiters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    
	public <T> List<T> queryAndMapList(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException {
        List<T> row = null;
        if(mapper != null) {
            HashMap<String,int[]> paramsCollection = new HashMap<String,int[]>();
            // normalize query paramiters an collect index
            sqlQuery = normalizeSql(sqlQuery,paramsCollection);
            // check if connected
            if (isConnected()) {
                PreparedStatement stmt =prepare(sqlQuery);
                buildParamitersIndex(params, paramsCollection, stmt);
                ResultSet results = stmt.executeQuery();

                if (results != null) {
                    while (results.next()) {
                        T singlerow = (T) mapper.map(results, 0);
                        if(row == null)
                            row = new ArrayList<T>();
                        row.add(singlerow);
                    }
                    if(!results.isClosed())
                    	results.close();
                }
            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }
        return row;
    }

    /**
     * Execute query and put results in json formatted string
     *
     * @param sqlQuery Query to execute
     * @param params   list of LuGMysqlParameter object that contains paramiters
     * @param mapper   mapper object
     * @return json string representing the result of the query
     * @throws SQLException
     */
    @Override
    public String queryJson(String sqlQuery, List<LuGMysqlParameter> params, ILuGRowMapper mapper) throws SQLException {
        List<Object> row = null;
        if(mapper != null) {
            HashMap<String,int[]> paramsCollection = new HashMap<String,int[]>();
            // normalize query paramiters an collect index
            sqlQuery = normalizeSql(sqlQuery,paramsCollection);
            // check if connected
            if (isConnected()) {
                PreparedStatement stmt = prepare(sqlQuery);
                buildParamitersIndex(params, paramsCollection, stmt);
                ResultSet results = stmt.executeQuery();

                if (results != null) {
                    while (results.next()) {
                        Object singlerow = mapper.map(results, 0);
                        if(row == null)
                            row = new ArrayList();
                        row.add(singlerow);
                    }
                    if(!results.isClosed())
                    	results.close();
                }
            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }

        return toJson(row);
    }

    /**
     * Return json representation of object
     *
     * @param row
     * @return
     */
    private String toJson(Object row) {
        Gson json = new Gson();
        String jresult ="";
        try{
            jresult = json.toJson(row);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  jresult;

    }

    /**
     * Execute query and put results in json formatted string
     *
     * @param sqlQuery Query to execute
     * @param params   array of object that contains paramiters
     * @param mapper   mapper object
     * @return json string representing the result of the query
     * @throws SQLException
     */
    @Override
    public String queryJson(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException {
        List<Object> row = null;
        if(mapper != null) {
            HashMap<String,int[]> paramsCollection = new HashMap<String,int[]>();
            // normalize query paramiters an collect index
            sqlQuery = normalizeSql(sqlQuery,paramsCollection);
            // check if connected
            if (isConnected()) {
                PreparedStatement stmt = prepare(sqlQuery);
                if (params != null && params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        stmt.setObject(i + 1, params[i]);
                    }
                }
                ResultSet results = stmt.executeQuery();

                if (results != null) {
                    while (results.next()) {
                        Object singlerow = mapper.map(results, 0);
                        if(row == null)
                            row = new ArrayList<>();
                        row.add(singlerow);
                    }
                    if(!results.isClosed())
                    	results.close();
                }
            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }

        return toJson(row);
    }

    /**
     * Execute query on alive connection and map results in List of object
     *
     * @param sqlQuery  query to execute
     * @param params    array of object that contains parameters
     * @param mapper    object that perform mapping of object
     * @param <T>       result object
     *
     * @return Object obtained from query or null if not found
     * @throws SQLException
     */
    public <T> List<T> queryAndMapList(String sqlQuery, Object[] params, ILuGRowMapper mapper) throws SQLException {
        List<T> row = null;
        if(mapper != null) {
            // check if connected
            if (isConnected()) {
                PreparedStatement stmt = prepare(sqlQuery);
                if (params != null && params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        stmt.setObject(i + 1, params[i]);
                    }
                }
                ResultSet results = stmt.executeQuery();

                if (results != null) {
                    while (results.next()) {
                        T singlerow = (T) mapper.map(results, 0);
                        if(row == null)
                            row = new ArrayList<T>();
                        row.add(singlerow);
                    }
                    if(!results.isClosed())
                    	results.close();
                }
            } else {
                new SQLException("Not connected db");
            }
        }
        else
        {
            new SQLException("You need to set mapper");
        }
        return row;
    }

    /**
     * Performs insert update or delete operations
     * The params are in list of LuGMysqlParameter
     *
     * @param sqlQuery  update, insert, delete query
     * @param params    list of LuGMysqlParameter objects
     * @return  numbers of affected rows
     *
     * @throws SQLException
     */
    public int update(String sqlQuery, List<LuGMysqlParameter> params) throws SQLException {
        int numRows = -1;
        HashMap<String,int[]> paramsCollection = new HashMap<String,int[]>();
        // normalize query paramiters an collect index
        sqlQuery = normalizeSql(sqlQuery,paramsCollection);
        // check if connected
        if (isConnected()) {
            PreparedStatement stmt = prepare(sqlQuery);
            buildParamitersIndex(params, paramsCollection, stmt);
            numRows = stmt.executeUpdate();
        } else {
            new SQLException("Not connected db");
        }
        return numRows;
    }

    /**
     * Performs insert update or delete operations
     *
     * @param sqlQuery  update, insert, delete query
     * @param params   array of objects contains parameters values
     *
     * @return  numbers of affected rows
     * @throws SQLException
     */
    public int update(String sqlQuery, Object[] params) throws SQLException {
        int numRows = -1;
        // check if connected
        if (isConnected()) {
            PreparedStatement stmt = prepare(sqlQuery);
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    stmt.setObject(i + 1, params[i]);
                }
            }
            numRows = stmt.executeUpdate();
        } else {
            new SQLException("Not connected db");
        }
        return numRows;
    }

    @Override
    public ResultSet callStoredProcedure(String procedureName, Object[] params) throws SQLException {
        ResultSet result = null;
        String paramiters = "";
        if(params != null && params.length>0){
            for(int j =0;j<params.length;j++){
                paramiters += "?,";
            }
            if(paramiters != null && !paramiters.isEmpty()){
                paramiters = paramiters.substring(0,paramiters.length()-1);
            }
        }
        String query = "{ call "+procedureName+"("+paramiters+") }";
        // check if connected
        if (isConnected()) {
            PreparedStatement  stmt = prepare(query);
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    stmt.setObject(i + 1, params[i]);
                }
            }
            result = stmt.executeQuery();
        } else {
            new SQLException("Not connected db");
        }
        return result;
    }

    /**
     * Get the PreparedStatement object
     * @return
     */
    public PreparedStatement  getPreparedStmt() {
        return preparedStmt;
    }

    /**
     * Set the PreparedStatement object
     *
     * @param preparedStmt
     */
    public void setPreparedStmt(PreparedStatement  preparedStmt) {
        this.preparedStmt = preparedStmt;
    }

    /**
     * Maps parameters that are in query into statement object
     *
     * @param params            list of parameters values
     * @param paramsCollection  List of mapping parameters and index value found in query
     * @param stmt              statement object
     * @throws SQLException
     */
    private void buildParamitersIndex(List<LuGMysqlParameter> params, HashMap<String, int[]> paramsCollection, PreparedStatement stmt) throws SQLException {
        if (params != null && params.size() > 0 && paramsCollection != null && paramsCollection.size()>0) {
            for (int i = 0; i < params.size(); i++) {
                LuGMysqlParameter itm = params.get(i);
                int[] listIndex = paramsCollection.get(itm.getName());
                if (listIndex != null && listIndex.length > 0) {
                    for (int j = 0; j < listIndex.length; j++) {
                        stmt.setObject(listIndex[j], itm.getValue());
                    }
                }
            }
        }
    }

    /**
     * Does the method replace the variables marked with @ in ? and get the indices
     *
     * @param sqlQuery          the sql query contains one more parameter having prefixed the character @
     * @param paramsCollection  the collection of parameters mapping name and indices
     * @return string sql that contains only parameters named with "?" character and fills object that contains indices mapping
     */
    private String normalizeSql(String sqlQuery, HashMap<String, int[]> paramsCollection) {
        String normalizedSql = sqlQuery;
        if(paramsCollection != null){
            Pattern paramPatter = Pattern.compile("\\@([\\w.$]+|\"[^\"]+\"|'[^']+')");
            Matcher matcher =  paramPatter.matcher(sqlQuery);

            if(matcher != null){
                // do somethings
                int index = 1;
                while (matcher.find()){
                    String mt =  matcher.group();
                    int list[] =  paramsCollection.get(mt);
                    if(list == null) {
                        list = new int[1];
                        list[0] = index;
                        paramsCollection.put(mt,list);
                    }
                    else
                    {
                        int[] nextList = new int[list.length+1];
                        for (int i=0;i<list.length;i++){
                            nextList[i]=list[i];
                        }
                        nextList[list.length] = index;
                        paramsCollection.put(mt,nextList);
                    }
                    normalizedSql = normalizedSql.replace(mt,"?");
                    index++;
                }
            }
        }
        return normalizedSql;
    }

}
