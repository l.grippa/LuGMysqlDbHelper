/**
 *   MIT License
 *
 *   Copyright (c) 2018 Luciano Grippa
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFT
 */
package eu.grippaweb.providers;

import eu.grippaweb.LuGMysqlDbHelper;
import eu.grippaweb.supports.LuGMysqlConfiguration;

import javax.sql.DataSource;

/**
 * Object used to create the instance for Helper interface
 *
 * -- ex: Instance from DataSource --
 * <code>
 *        dataSource = new MysqlDataSource();
 *        dataSource.setDatabaseName("mysqltest");
 *        dataSource.setUser("user");
 *        dataSource.setPassword("user");
 *        dataSource.setPort(3306);
 *        dataSource.setServerName("localhost");
 *
 *        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(dataSource);
 *
 * </code>
 *
 * @author Luciano Grippa
 * @since 1.0.0
 */
public class LuGMysqlProvider {

    private static LuGMysqlDbHelper dbSingleton;
    private LuGMysqlProvider(){}

    /**
     * Create istance of LuGMysqlDbHelper from DataSource object
     *
     * @param ds Datasource
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper initMyHelper(DataSource ds)
    {
        LuGMysqlConfiguration configuration = new LuGMysqlConfiguration();
        configuration.setDataSource(ds);
        LuGMysqlDbHelper dbHelper = new LuGMysql();
        dbHelper.setConfiguration(configuration);
        return  dbHelper;
    }

    /**
     * Create istance of LuGMysqlDbHelper from LuGMysqlConfiguration object
     *
     * @param configuration LuGMysqlConfiguration object
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper initMyHelper(LuGMysqlConfiguration configuration)
    {
       LuGMysqlDbHelper dbHelper = new LuGMysql();
       dbHelper.setConfiguration(configuration);
       return  dbHelper;
    }

    /**
     * Create istance of LuGMysqlDbHelper from complete jdbc mysql string url
     *
     * @param mysql_url_complete string of jdbc connection complete with username e password
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper initMyHelper(String mysql_url_complete) {
        LuGMysqlConfiguration configuration = new LuGMysqlConfiguration();
        configuration.setConnectionString(mysql_url_complete);

        LuGMysqlDbHelper dbHelper = new LuGMysql();
        dbHelper.setConfiguration(configuration);
        return  dbHelper;
    }

    /**
     * Create istance of LuGMysqlDbHelper from jdbc mysql string url without username and password specified
     *
     * @param mysql_url_no_password string of jdbc connection complete without username e password
     * @param username
     * @param password
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper initMyHelper(String mysql_url_no_password,String username,String password) {
        LuGMysqlConfiguration configuration = new LuGMysqlConfiguration();
        configuration.setConnectionString(mysql_url_no_password);
        configuration.setUsername(username);
        configuration.setPassword(password);

        LuGMysqlDbHelper dbHelper = new LuGMysql();
        dbHelper.setConfiguration(configuration);
        return  dbHelper;
    }

    /**
     * Create Singleton istance of LuGMysqlDbHelper from LuGMysqlConfiguration object
     *
     * @param configuration LuGMysqlConfiguration object
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper initMyHelperAsSingleton(LuGMysqlConfiguration configuration) {
        if(dbSingleton == null)
            dbSingleton = new LuGMysql();
        dbSingleton.setConfiguration(configuration);
        return dbSingleton;
    }

    /**
     * Get Singleton istance of LuGMysqlDbHelper
     * before call this method should inizialize istance with initMyHelperAsSingleton method
     *
     * @return istance of LuGMysqlDbHelper
     */
    public static LuGMysqlDbHelper getMyHelperAsSinglton() {
       return dbSingleton;
    }
}