# LuGMysqlDbHelper
This is a simple library that helps perform query operations using Mysql Connector/J and JDBC API.<br />
No complex frameworks have been used, but standard java APIs so that editing and implementation are easy.

# Requirements and install
LuGMysqlDbHelper is compiled with JDK 1.7 but also works with later versions.<br/>
It is a maven project so after download run this command:
```sh
mvn clean install 
```
and include the library into your project through the pom.xml file:
```xml
<dependency>
    <groupId>eu.grippaweb.LuGMysqlDbHelper</groupId>
    <artifactId>LuGMysqlDbHelper</artifactId>
    <version>1.0.0</version>
</dependency>
```
Or compile it using any IDE which supports the maven project like Eclipse or IntelliJ.<br />
Or copying  **LuGMyDbHelper.jar** into your build path. The jar is located in project **/target/** directory.
# How to use it
Find all examples in **src/test/java/eu/grippaweb** directory.
The database schema used for examples is named **mysqltest** and table **employee** definition is:
```sql
 CREATE TABLE mysqltest.employee(
   id INT(11) NOT NULL AUTO_INCREMENT,
   first_name VARCHAR(20) DEFAULT NULL,
   last_name VARCHAR(20) DEFAULT NULL,
   salary INT(11) DEFAULT NULL,
   birth_date DATE DEFAULT NULL,
   PRIMARY KEY (id)
 )
```
the emplyee class entity is
```java
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    private Date birthDate;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public int getSalary() {
        return salary;
    }
    
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    public Date getBirthDate() {
        return birthDate;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
```
and mapper class is:
```java
public class EmployeeMapper implements ILuGRowMapper<Employee> {
    @Override
    public Employee map(ResultSet rs, int index) throws SQLException {
        Employee emp = new Employee();
        emp.setId(rs.getInt("id"));
        emp.setFirstName(rs.getString("first_name"));
        emp.setLastName(rs.getString("last_name"));
        emp.setSalary(rs.getInt("salary"));
        emp.setBirthDate(rs.getDate("birth_date"));
        return emp;
    }
}
```
### EXAMPLE 1: 
#### connect using datasource:
```java
public class TestApp {
    public static void main(){
        
        // build datasource or you can retrieve it with jndi
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setDatabaseName("mysqltest");
        dataSource.setUser("user");
        dataSource.setPassword("user");
        dataSource.setPort(3306);
        dataSource.setServerName("localhost");
        ///////////////////////////////////////////////////////////
        // init helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(dataSource);
        // init employ entity
        Employee emp = null;
        try {
             // connect database
             dbHelper.connect();
             if(dbHelper.isConnected()){
                // execute some query in this case map results in employee object
                emp = dbHelper.queryAndMap("SELECT * FROM employee WHERE first_name=? and last_name=? LIMIT 1",new Object[]{"Luciano","Grippa"},new EmployeeMapper());
                System.out.println("ID: "+emp.getId()+
                                   "|    FIRST NAME: "+emp.getFirstName()+
                                   "|    LAST NAME: "+emp.getLastName()+
                                   "|    SALARY: "+emp.getSalary()+
                                   "|    BIRTH DATE "+emp.getBirthDate());
             }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // close connection
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```
### EXAMPLE 2: 
#### connect using configuration class and list the employees with the name = Luciano:
```java
public class TestApp {
    public static void main(){
        
        // set configuration class //////////////////////////////////////
        LuGMysqlConfiguration configuration = new LuGMysqlConfiguration();
        configuration.setConnectionString("jdbc:mysql://localhost:3306/mysqltest?useSSL=true&serverTimezone=Europe/Rome");
        configuration.setUsername("user");
        configuration.setPassword("user");
        configuration.setDriver("com.mysql.cj.jdbc.Driver");
        //////////////////////////////////////////////////////////////////////////
        // init helper //////////////////////////////////////////////////////////
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(configuration);
        // init employ entity  //////////////////////////////////////////////////
        List<Employee> emp = null;
        try {
             // connect database
             dbHelper.connect();
             if(dbHelper.isConnected()){
                // execute some query in this case map results in employee object list
                emp = dbHelper.queryAndMapList("SELECT * FROM employee WHERE first_name=?",new Object[]{"Luciano"},new EmployeeMapper());
                for(int j =0;j<emp.size();j++){
                    Employee e = emp.get(j);
                    System.out.println("ID: "+e.getId()+
                                       "|   FIRST NAME: "+e.getFirstName()+
                                       "|   LAST NAME: "+e.getLastName()+
                                       "|   SALARY: "+e.getSalary()+
                                       "|   BIRTH DATE: "+e.getBirthDate());
    
                }
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // close connection
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```
### EXAMPLE 3:
####  Update or insert query
```java
public class TestApp {
    public static void main(){
        
        // set configuration class //////////////////////////////////////
        LuGMysqlConfiguration configuration = new LuGMysqlConfiguration();
        // set up your connection data
        configuration.setConnectionString("jdbc:mysql://localhost:3306/mysqltest?useSSL=true&serverTimezone=Europe/Rome");
        configuration.setUsername("user");
        configuration.setPassword("user");
        configuration.setDriver("com.mysql.cj.jdbc.Driver");
        ////////////////////////////////////////////////////////////////
        
        // init helper //////////////////////////////////////////////////////////
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(configuration);
        try {
             // connect database
             dbHelper.connect();
             if(dbHelper.isConnected()){
                  /// insert or update is the same for dbHelper.update
                  String sqlUpdate ="UPDATE employee SET first_name=? WHERE id=?";
                  int numRows = dbHelper.update(sqlUpdate,new Object[]{"Derek",4});
                  boolean isUpdate = numRows>0;
                  System.out.println(isUpdate? "Update correct!!":"Error update");
             }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // close connection
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```
### EXAMPLE 4:
####  list results using hashmap mapper
This example explains how to list query results but will not won't use a specifics mapper:
```java
public class TestApp {
     public static void main(){
         // build helper
         LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
         try {
             // connect database
             if (dbHelper.connect()) {
                 // successfully connect
                 // query list of employees
                 List<HashMap<String,Object>> resultlist = dbHelper.queryAndMapList("SELECT * FROM employee;", new Object[]{}, new HashMapMapper());
                 
                 if(resultlist != null && resultlist.size()>0){
                     
                     Iterator rs=  resultlist.iterator();
                     /*
                        the results list contains key value pairs, 
                        where keys are field's name and values are values of field in specific row index
                      */
                     while (rs.hasNext()){
                         HashMap<String,Object> mapEntry = (HashMap<String, Object>) rs.next();
                         System.out.println("ID: "+mapEntry.get("id")+" ");
                         System.out.print("First name: "+mapEntry.get("first_name")+" ");
                         System.out.print("Last name: "+mapEntry.get("last_name")+" ");
                         System.out.print("Salary: "+mapEntry.get("salary")+" ");
                         System.out.print("Birth date: "+mapEntry.get("birth_date")+" | ");
                     }
                 }
 
             } else {
                 Assert.assertTrue("No database connected", false);
             }
         } catch (LuGNoConfigurationException e) {
             e.printStackTrace();
             Assert.fail();
         } catch (SQLException e) {
             e.printStackTrace();
             Assert.fail();
         } finally {
             if (dbHelper != null) {
                 try {
                     dbHelper.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                     Assert.fail();
                 }
             }
         }
     }
}
```

### EXAMPLE 5:
####  get results into json string
This example explains how to list query results into json formatted string , the json representing an array of object like as:
```json
[{
		"id": 1,
		"firstName": "Luciano",
		"lastName": "Grippa",
		"salary": 30000,
		"birthDate": "giu 7, 1975"
	}, {
		"id": 2,
		"firstName": "Jorge",
		"lastName": "Monti",
		"salary": 125444,
		"birthDate": "gen 3, 1951"
	}, {
		"id": 3,
		"firstName": "Gigi",
		"lastName": "Carli",
		"salary": 7777,
		"birthDate": "feb 23, 1984"
	}, {
		"id": 4,
		"firstName": "Derek",
		"lastName": "Marx",
		"salary": 99999,
		"birthDate": "feb 3, 1971"
	}
]
```
The java code is:
```java
public class TestApp {
    public static void main() {
        // build helper
        LuGMysqlDbHelper dbHelper = LuGMysqlProvider.initMyHelper(MYSQL_URL_COMPLETE);
        // init result string
        String jemp = null;
        try {
            // connect database
            if(dbHelper.connect()){
                // query for json results
                jemp = dbHelper.queryJson("SELECT * FROM employee",new Object[]{},new EmployeeMapper());
                if(jemp != null && !jemp.isEmpty()){
                    // show string in output
                    System.out.println(jemp);
                }
            }
            else
            {
                Assert.assertTrue("No database connected",false);
            }
        } catch (LuGNoConfigurationException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }finally {
            if(dbHelper != null){
                try {
                    dbHelper.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        }
    }
}
```